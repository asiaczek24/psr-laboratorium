package psk;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Stream;

import lombok.Data;
import psk.actions.Action;
import psk.actions.ActionChooser;
import psk.actions.ExitAction;
import psk.actions.PrzewoznikAction;
import psk.datastore.BasicDatastore;
import psk.datastore.RedisDatastore;
import psk.model.Przewoznik;
import psk.query.Query;

@Data
public final class App {
    private BasicDatastore datastore;
    List<Action> actions;
    Scanner in;

    private App() {
        actions = new ArrayList<>();
    }

    public static void main(String[] args) {
        App app = initApp(args);
        app.start();
    }

    private static App initApp(String[] args) {
        App app = new App();
        Optional<BasicDatastore> oData = new RedisDatastore().getInstance();

        oData.ifPresentOrElse(data -> {
            app.setDatastore(data);
        }, () -> {
            System.out.println("System could not initialize datastore!");
            System.exit(0);
        });

        Scanner in = new Scanner(System.in);
        app.setIn(in);
        app.registerPersistentClasses(Przewoznik.class);
        app.initMenu();
        Query.init(in);

        return app;
    }

    public void registerPersistentClasses(Class<?>... classes) {
        Stream.of(classes).forEach(c -> {
            datastore.initStore(c);
        });
    }

    public void initMenu() {
        actions.add(new ExitAction(datastore));
        actions.add(new PrzewoznikAction(datastore, in));
    }

    public void start() {
        ActionChooser ac = new ActionChooser(actions, in, false);
        ac.start();
    }
}
