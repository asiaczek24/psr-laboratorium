package psk.actions;

import psk.datastore.BasicDatastore;

abstract public class Action {

    protected String name;
    protected String description;
    protected BasicDatastore datastore;

    protected Action(String name, String description, BasicDatastore datastore) {
        this.name = name;
        this.description = description;
        this.datastore = datastore;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void showName() {
        System.out.println(name);
    }

    public void showDescription() {
        System.out.println(description);
    }

    @Override
    public String toString() {
        return String.format("%s : %s", name, description);
    }

    public abstract void execute();
}