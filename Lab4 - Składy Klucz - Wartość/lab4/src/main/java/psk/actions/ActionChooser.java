package psk.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ActionChooser {

    List<Action> actions;
    Scanner in;

    public ActionChooser(boolean addReturn) {
        actions = new ArrayList<>();

        if (addReturn) {
            actions.add(new ReturnAction());
        }
    }

    public ActionChooser(List<Action> actions, Scanner in, boolean addReturn) {
        this(addReturn);
        this.in = in;
        this.actions.addAll(actions);
    }

    public void start() {
        while (true) {
            showMenu();
            int option = chooseOption();
            if (returnMenu(option)) {
                break;
            }
            executeAction(option);
        }
    }

    private void showMenu() {
        actions.forEach(a -> {
            System.out.printf("[%2d]. %s\n", actions.indexOf(a), a.toString());
        });
    }

    private int chooseOption() {
        while (true) {
            System.out.print("Choose option: ");
            try {
                int option = in.nextInt();
                if (option >= 0 && option < actions.size()) {
                    return option;
                } else {
                    System.out.println("Wrong option, try again!");
                }
            } catch (Exception e) {
                System.out.println("Input needs to be a number, try again!");
                in.nextLine();
            }
        }
    }

    private boolean returnMenu(int n) {
        return ReturnAction.NAME.equalsIgnoreCase(actions.get(n).getName());
    }

    private void executeAction(int n) {
        try {
            actions.get(n).execute();
        } catch (Exception e) {
            actions.stream().filter(a -> ExitAction.name.equalsIgnoreCase(a.getName())).findAny()
                    .ifPresent(Action::execute);
        }
    }
}