package psk.actions;

import psk.datastore.BasicDatastore;

public class ExitAction extends Action {
    static final String name = "Exit";
    static final String description = "exits from program";

    public ExitAction(BasicDatastore datastore) {
        super(name, description, datastore);
    }

    @Override
    public void execute() {
        super.datastore.closeDataStore();
        System.exit(0);
    }

}