package psk.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import psk.actions.przewoznik.AddPrzewoznikAction;
import psk.actions.przewoznik.DeletePrzewoznikAction;
import psk.actions.przewoznik.EditPrzewoznikAction;
import psk.actions.przewoznik.SearchPrzewoznikAction;
import psk.actions.przewoznik.ShowPrzewoznikAction;
import psk.datastore.BasicDatastore;

public class PrzewoznikAction extends Action {
    private final static String NAME = "Przewoznik management";
    private final static String DESCRIPTION = "manage przewoznik by adding, showing, updating, deleting entries";

    ActionChooser actionChooser;

    public PrzewoznikAction(BasicDatastore datastore, Scanner in) {
        super(NAME, DESCRIPTION, datastore);
        List<Action> actions = new ArrayList<>();
        actions.add(new AddPrzewoznikAction(datastore, in));
        actions.add(new ShowPrzewoznikAction(datastore, in));
        actions.add(new SearchPrzewoznikAction(datastore, in));
        actions.add(new EditPrzewoznikAction(datastore, in));
        actions.add(new DeletePrzewoznikAction(datastore, in));
        actionChooser = new ActionChooser(actions, in, true);
    }

    @Override
    public void execute() {
        actionChooser.start();
    }

}
