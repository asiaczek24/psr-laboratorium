package psk.actions;

public class ReturnAction extends Action {
    public static final String NAME = "Return";
    static final String DESCRIPTION = "returns one lvl up in menu";

    public ReturnAction() {
        super(NAME, DESCRIPTION, null);
    }

    @Override
    public void execute() {
    }
}