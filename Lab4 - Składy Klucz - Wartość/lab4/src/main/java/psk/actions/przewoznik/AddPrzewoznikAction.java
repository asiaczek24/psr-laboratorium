package psk.actions.przewoznik;

import java.util.Optional;
import java.util.Scanner;

import psk.actions.Action;
import psk.datastore.BasicDatastore;
import psk.model.Przewoznik;

public class AddPrzewoznikAction extends Action {
    static final String name = "Add przewoznik";
    static final String description = "adds przewoznik";
    Scanner in;

    public AddPrzewoznikAction(BasicDatastore datastore, Scanner in) {
        super(name, description, datastore);
        this.in = in;
    }

    @Override
    public void execute() {
        in.nextLine();
        System.out.print("ID: ");
        String id = in.nextLine();
        System.out.print("Name of przewoznik: ");
        String name = in.nextLine();
        System.out.print("Surname of przewoznik: ");
        String surname = in.nextLine();

        Optional<Przewoznik> oPrzewoznik = datastore.save(new Przewoznik(id, name, surname), Przewoznik.class);
        oPrzewoznik.ifPresentOrElse(p -> {
            System.out.printf("Value %s exists in database, please use update\n", p.toString());
        }, () -> {
            System.out.println("Writing to database successfull");
        });
    }
}
