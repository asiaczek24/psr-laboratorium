package psk.actions.przewoznik;

import java.util.Optional;
import java.util.Scanner;

import psk.actions.Action;
import psk.datastore.BasicDatastore;
import psk.model.Przewoznik;

public class DeletePrzewoznikAction extends Action {
    static final String name = "Delete przewoznik";
    static final String description = "deletes przewoznik";
    Scanner in;
    ShowPrzewoznikAction showPrzewoznik;

    public DeletePrzewoznikAction(BasicDatastore datastore, Scanner in) {
        super(name, description, datastore);
        this.in = in;
    }

    @Override
    public void execute() {
        datastore.findAllMap(Przewoznik.class).forEach((k, p) -> {
            System.out.println(p.toString());
        });

        in.nextLine();
        System.out.print("Give id to remove: ");
        String id = in.nextLine();

        Optional<Przewoznik> oP = datastore.delete(id, Przewoznik.class);
        oP.ifPresentOrElse((p -> {
            System.out.printf("Deleted: %s", p.toString());
        }), () -> {
            System.out.println("Could not find record to delete");
        });
    }
}
