package psk.actions.przewoznik;

import java.util.Map;
import java.util.Scanner;

import psk.actions.Action;
import psk.datastore.BasicDatastore;
import psk.model.Persistable;
import psk.model.Przewoznik;

public class EditPrzewoznikAction extends Action {
    static final String name = "Edit przewoznik";
    static final String description = "edits pliceman information";
    Scanner in;

    public EditPrzewoznikAction(BasicDatastore datastore, Scanner in) {
        super(name, description, datastore);
        this.in = in;
    }

    @Override
    public void execute() {
        Map<Object, Persistable> data = datastore.findAllMap(Przewoznik.class);
        data.forEach((key, value) -> {
            System.out.println(value.toString());
        });

        in.nextLine();
        System.out.print("Choose ID to edit: ");
        String id = in.nextLine();
        System.out.print("New name [empty if no change]: ");
        String name = in.nextLine();
        System.out.print("New surname [empty if no change]: ");
        String surname = in.nextLine();

        Persistable tmp = data.get(id);
        if (!(tmp instanceof Przewoznik)) {
            System.out.println("Nothing to update!");
        }

        Przewoznik pTmp = (Przewoznik) tmp;
        name = name.isBlank() ? pTmp.getName() : name;
        surname = surname.isBlank() ? pTmp.getLastname() : surname;

        datastore.update(new Przewoznik(id, name, surname), Przewoznik.class);
    }
}
