package psk.actions.przewoznik;

import java.util.Collection;
import java.util.Scanner;

import psk.actions.Action;
import psk.datastore.BasicDatastore;
import psk.model.Przewoznik;
import psk.query.Query;

public class SearchPrzewoznikAction extends Action {
    static final String name = "Search przewoznik";
    static final String description = "advanced search criteria";
    Scanner in;

    public SearchPrzewoznikAction(BasicDatastore datastore, Scanner in) {
        super(name, description, datastore);
        this.in = in;
    }

    @Override
    public void execute() {
        Query query = new Przewoznik("").advancedSearch();
        Collection<Przewoznik> p = datastore.advancedSearch(query);
        p.forEach(System.out::println);
    }

}
