package psk.actions.przewoznik;

import java.util.Scanner;

import psk.actions.Action;
import psk.datastore.BasicDatastore;
import psk.model.Przewoznik;

public class ShowPrzewoznikAction extends Action {
    static final String name = "Show przewoznik";
    static final String description = "shows detailed information about przewoznik";
    Scanner in;

    public ShowPrzewoznikAction(BasicDatastore datastore, Scanner in) {
        super(name, description, datastore);
        this.in = in;
    }

    @Override
    public void execute() {
        datastore.findAllMap(Przewoznik.class).forEach((id, przewoznik) -> {
            System.out.println(przewoznik.toString());
        });
    }
}
