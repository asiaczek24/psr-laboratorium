package psk.datastore;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import psk.model.Persistable;
import psk.query.Query;

public interface BasicDatastore {

    default <T extends Persistable> Optional<T> save(T obj, Class<?> className) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    default <T extends Persistable> Optional<T> update(T obj, Class<?> className) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    default <T extends Persistable> Optional<T> delete(Object key, Class<?> className)
            throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    default <T extends Persistable> Optional<T> find(Object primaryKey, Class<?> className)
            throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    default <K, V extends Persistable> Map<K, V> findAllMap(Class<?> className) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    default void closeDataStore() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    default <T extends Persistable> Collection<T> advancedSearch(Query query) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    default void initStore(Class<?> className) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    Optional<BasicDatastore> getInstance();

    Optional<BasicDatastore> getInstance(boolean test);
}