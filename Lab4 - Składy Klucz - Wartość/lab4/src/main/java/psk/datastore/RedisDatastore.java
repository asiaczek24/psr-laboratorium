package psk.datastore;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.api.RMap;

import psk.model.Persistable;
import psk.query.Query;

public class RedisDatastore implements BasicDatastore {
    private static RedissonClient redis;

    public <T extends Persistable> Optional<T> save(T obj, Class<?> className) throws UnsupportedOperationException {
        Map<String, T> store = redis.getMap(className.getName());
        return Optional.ofNullable(store.put(obj.getKey(), obj));
    }

    public <T extends Persistable> Optional<T> update(T obj, Class<?> className) throws UnsupportedOperationException {
        Map<String, T> store = redis.getMap(className.getName());
        if (!store.containsKey(obj.getKey())) {
            return Optional.empty();
        }

        return Optional.ofNullable(store.put(obj.getKey(), obj));
    }

    public <T extends Persistable> Optional<T> delete(Object key, Class<?> className)
            throws UnsupportedOperationException {
        Map<String, T> store = redis.getMap(className.getName());
        if (!store.containsKey(key)) {
            return Optional.empty();
        }

        return Optional.ofNullable(store.remove(key));
    }

    public <T extends Persistable> Optional<T> find(Object primaryKey, Class<?> className)
            throws UnsupportedOperationException {
        Map<String, T> store = redis.getMap(className.getName());
        return Optional.ofNullable(store.remove(primaryKey));
    }

    public <K, V extends Persistable> Map<K, V> findAllMap(Class<?> className) throws UnsupportedOperationException {
        return redis.getMap(className.getName());
    }

    public void closeDataStore() throws UnsupportedOperationException {
        redis.shutdown();
        redis = null;
    }

    public <T extends Persistable> Collection<T> advancedSearch(Query query) throws UnsupportedOperationException {
        RMap<String, T> store = redis.getMap(query.getClassName().getName());
        Predicate<Persistable> persistablePredicate = generatePredicate(query);
        return store.values().stream().filter(persistablePredicate).collect(Collectors.toSet());
    }

    private <T extends Persistable> Predicate<T> generatePredicate(Query query) {
        Predicate<T> finalPredicate = new Predicate<>() {
            Class<? extends Persistable> c = query.getClassName();

            @Override
            public boolean test(T t) {
                if (c.isInstance(t)) {
                    return true;
                }

                return true;
            }

        };

        Predicate<String> p1 = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.startsWith("a");
            }
        };

        Predicate<String> p2 = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.endsWith("a");
            }
        };

        p1.and(p2);

        List<Field> f = List.of(query.getFields());

        List<String> conditions = query.getQuery();
        for (int i = 0; i < conditions.size(); i += 3) {
            String first = conditions.get(i);
            if (first.equals(Query.WHERE) || first.equals(Query.AND) || first.equals(Query.OR)) {
                i += 1;
            }

            String s1 = conditions.get(i);
            String s2 = conditions.get(i + 1);
            String s3 = conditions.get(i + 2);

            Optional<Field> first1 = f.stream().filter(field -> field.getName().equalsIgnoreCase(s1)).findFirst();
            Optional<Predicate<T>> oPredicate = first1.map(field -> {
                return new Predicate<T>() {
                    String condition = s2;

                    @Override
                    public boolean test(T t) {
                        try {
                            Object o = "'" + field.get(t) + "'";
                            boolean equals = s3.equals(o);
                            if (condition.equals(Query.EQUALS)) {
                                return equals;
                            }
                            return !equals;
                        } catch (IllegalArgumentException | IllegalAccessException e) {
                            return false;
                        }
                    }
                };
            });

            if (oPredicate.isPresent()) {
                if (i == 1 || s1.equals(Query.AND)) {
                    finalPredicate = finalPredicate.and(oPredicate.get());
                } else {
                    finalPredicate.or(oPredicate.get());
                }
            }
        }

        return finalPredicate;
    }

    public void initStore(Class<?> className) {
    }

    @Override
    public Optional<BasicDatastore> getInstance() {
        return getInstance(false);
    }

    @Override
    public Optional<BasicDatastore> getInstance(boolean test) {
        if (redis == null) {
            Config config = new Config();
            config.useSingleServer().setAddress("redis://192.168.0.13:5701");
            redis = Redisson.create(config);
        }
        return Optional.of(this);
    }
}
