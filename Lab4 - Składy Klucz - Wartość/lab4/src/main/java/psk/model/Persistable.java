package psk.model;

import java.io.Serializable;

abstract public class Persistable implements Serializable {
    private static final long serialVersionUID = 1L;

    protected String key;

    public String getKey() {
        return key;
    }

    protected void setKey(String key) {
        this.key = key;
    }
}