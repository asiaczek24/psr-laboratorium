package psk.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import psk.query.Query;

@Data
@EqualsAndHashCode(callSuper = false)
public class Przewoznik extends Persistable implements Searchable {
    private static final long serialVersionUID = 1L;
    public String name;
    public String lastname;

    public Przewoznik(String key) {
        super();
        super.setKey(key);
    }

    public Przewoznik(String key, String name, String lastname) {
        this(key);
        this.name = name;
        this.lastname = lastname;
    }

    @Override
    public String toString() {
        return String.format("Przewoznik #%s: %s %s", key, name, lastname);
    }

    @Override
    public Query advancedSearch() {
        return Query.generate(Przewoznik.class).build();
    }
}
