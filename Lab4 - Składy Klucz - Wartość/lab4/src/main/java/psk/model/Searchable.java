package psk.model;

import psk.query.Query;

public interface Searchable {
    Query advancedSearch();
}