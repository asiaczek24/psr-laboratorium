package psk.query;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Scanner;

import lombok.Data;
import psk.model.Persistable;

@Data
public class Query {
    private static Scanner in;

    public static final String WHERE = "where";
    public static final String AND = "and";
    public static final String OR = "or";
    public static final String EQUALS = "=";
    public static final String NOT_EQUALS = "!=";
    public static final String IS_NULL = "is null";
    public static final String NOT_NULL = "not null";

    private Field[] fields;
    private List<String> query;
    private Class<? extends Persistable> className;

    Query(Class<? extends Persistable> className, Field[] fields, List<String> query) {
        this.query = query;
        this.fields = fields;
        this.className = className;
    }

    public static void init(Scanner in) {
        Query.in = in;
    }

    public static QueryBuilder generate(Class<? extends Persistable> className) {
        return new QueryBuilder(className, in);
    }
}
