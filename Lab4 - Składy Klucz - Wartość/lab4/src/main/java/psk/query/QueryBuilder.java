package psk.query;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

import psk.model.Persistable;

public class QueryBuilder {
    private Scanner in;

    private static int tmp = 0;
    Class<? extends Persistable> className;
    Field[] fields;
    List<String> queryList;

    public QueryBuilder(Class<? extends Persistable> className, Scanner in) {
        this.className = className;
        this.in = in;
        fields = className.getDeclaredFields();
        queryList = new ArrayList<>(fields.length);
    }

    public Query build() {
        func1();
        return new Query(className, fields, queryList);

    }

    private void func1() {
        build: while (true) {
            System.out.println("[ 0]. End query building");
            System.out.println("[ 1]. Append WHERE");
            System.out.println("[ 2]. Append AND");
            System.out.println("[ 3]. Append OR");
            System.out.println("[ 4]. Append IS NULL");
            System.out.println("[ 5]. Append NOT NULL");
            int option = in.nextInt();
            switch (option) {
                case 0:
                    break build;
                case 1:
                    where();
                    break;
                case 2:
                    and();
                    break;
                case 3:
                    or();
                    break;
                case 4:
                    addNull();
                    break;
                case 5:
                    notNull();
                    break;
                default:
                    System.out.println("Wrong option!");
                    continue build;
            }
            func2();
        }
    }

    private void func2() {
        while (true) {
            System.out.println("[ 0]. Cancel");
            tmp = 1;
            Stream.of(fields).map(f -> String.format("[%2d]. %s", tmp++, f.getName())).forEach(System.out::println);

            int option = in.nextInt();
            if (option == 0) {
                queryList.remove(queryList.size() - 1);
                break;
            }
            if (option < 1 || option > fields.length) {
                System.out.println("Wrong option!");
                continue;
            }
            queryList.add(fields[option - 1].getName());
            break;
        }

        func3();
    }

    private void func3() {
        build: while (true) {
            System.out.println("[ 0]. Cancel");
            System.out.println("[ 1]. EQUALS");
            System.out.println("[ 2]. NOT EQUALS");
            System.out.println();
            int option = in.nextInt();
            if (option == 0) {
                queryList.remove(queryList.size() - 1);
                queryList.remove(queryList.size() - 1);
            }
            switch (option) {
                case 1:
                    equals();
                    break build;
                case 2:
                    equals();
                    break build;
            }
        }

        in.nextLine();
        System.out.print("value: ");
        add("'" + in.nextLine() + "'");
    }

    public void where() {
        add(Query.WHERE);
    }

    public void and() {
        add(Query.AND);
    }

    public void or() {
        add(Query.OR);
    }

    public void addNull() {
        add(Query.IS_NULL);
    }

    public void notNull() {
        add(Query.NOT_NULL);
    }

    public void equals() {
        add(Query.EQUALS);
    }

    public void notEquals() {
        add(Query.NOT_EQUALS);
    }

    private void add(String s) {
        queryList.add(s);
    }
}