package pl.kielce.tu.mongodb;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.elemMatch;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.exists;
import static com.mongodb.client.model.Filters.gt;
import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Filters.or;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Updates.inc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

public class TestMongoDB {
	public static void main(String[] args)
	{

		String user = "student01";
		String password = "student01";
		String host = "localhost";
		int port = 27017;
		String database = "database01";

		String clientURI = "mongodb://" + user + ":" + password + "@" + host + ":" + port + "/" + database;
		MongoClientURI uri = new MongoClientURI(clientURI);

		MongoClient mongoClient = new MongoClient(uri);

		MongoDatabase db = mongoClient.getDatabase(database);

		db.getCollection("people").drop();

		MongoCollection<Document> collection = db.getCollection("people");


		int wybor = -1;
		db.getCollection("ksiazki").drop();
		db.getCollection("employees").drop();
		MongoCollection<Document> ksiazki = db.getCollection("ksiazki");
		MongoCollection<Document> employees = db.getCollection("employees");
		int id_ksiazki = 1;
		int id_pracownika = 1;
		while(wybor != 0){
			System.out.println("Wybierz 0 aby zamknąć\n1 dodawanie\n" +
					"2 wyszukiwanie \n3 usuwanie\n" +
					"4 aktualizowanie");

			Scanner scanner = new Scanner(System.in);
			wybor = Integer.parseInt(scanner.nextLine());
			if(wybor == 0){

				break;

			}
			else if(wybor == 1){
				wybor = -1;
				System.out.println("Wybierz\n1 dla ksiazki\n2 dla pracownika");
				wybor = Integer.parseInt(scanner.nextLine());
				if(wybor == 1){
					System.out.println("Wpisz imie autora");
					String imie = scanner.nextLine();
					System.out.println("Wpisz nazwisko autora");
					String nazwisko = scanner.nextLine();
					System.out.println("Wpisz tytul ksiazki");
					String tytul = scanner.nextLine();

					wybor = -1;
					Document ksiazka = new Document("_id", id_ksiazki)
							.append("imie", imie)
							.append("nazwisko", nazwisko)
							.append("tytul", tytul);
					ksiazki.insertOne(ksiazka);
					id_ksiazki += 1;
				}
				else if(wybor == 2){
					System.out.println("Wpisz imie");
					String imie = scanner.nextLine();
					System.out.println("Wpisz nazwisko");
					String nazwisko = scanner.nextLine();
					System.out.println("Wpisz PESEL");
					String pesel = scanner.nextLine();
					wybor = -1;
					Document pracownik = new Document("_id", id_pracownika)
							.append("imie", imie)
							.append("nazwisko", nazwisko)
							.append("PESEL", pesel);
					employees.insertOne(pracownik);
					id_pracownika += 1;
				}
			}
			else if(wybor == 2){
				wybor = -1;
				System.out.println("Wybierz\n1 dla wszystkich ksiazek\n2 dla wszystkich pracownikow" +
						"\n3 dla ksiazki po id\n4 dla pracownika po id\n5 dla agregacji ");
				wybor = Integer.parseInt(scanner.nextLine());
				if(wybor == 1){
					for (Document studs : ksiazki.find())
						System.out.println("find() " + studs.toJson());
				}
				else if(wybor == 2){
					for (Document emp : employees.find())
						System.out.println("find() " + emp.toJson());
				}
				else if(wybor == 3){
					System.out.println("Wpisz id");
					int id = Integer.parseInt(scanner.nextLine());
					for (Document studs : ksiazki.find(or(eq("_id", id))))
						System.out.println(studs.toJson());

				}
				else if(wybor == 4){
					System.out.println("Wpisz id");
					int id = Integer.parseInt(scanner.nextLine());
					for (Document emp : employees.find(or(eq("_id", id))))
						System.out.println(emp.toJson());
				}
				wybor = -1;
			}
			else if(wybor == 3){
				wybor = -1;
				System.out.println("Wybierz\n1 aby usunac ksiazke\n2 aby usunac pracownika");
				wybor = Integer.parseInt(scanner.nextLine());
				if(wybor == 1){
					System.out.println("Wpisz id ksiazki");
					int id = Integer.parseInt(scanner.nextLine());
					ksiazki.deleteOne(eq("_id", id));
					System.out.println("Ksiazka o id " + id + " Usunięta");
				}
				else if(wybor == 2){
					System.out.println("Wpisz id pracownika");
					int id = Integer.parseInt(scanner.nextLine());
					employees.deleteOne(eq("_id", id));
					System.out.println("Pracownik o id " + id + " Usunięty");
				}
			}
			else if(wybor == 4){
				wybor = -1;
				System.out.println("Wybierz\n1 aby zaktualizowac ksiazke\n2 aby zaktualizowac pracownika");
				wybor = Integer.parseInt(scanner.nextLine());
				if(wybor==1){
					System.out.println("Wpisz id ksiazki");
					int id = Integer.parseInt(scanner.nextLine());

					System.out.println("Wpisz imie");
					String imie = scanner.nextLine();
					System.out.println("Wpisz nazwisko");
					String nazwisko = scanner.nextLine();
					System.out.println("Wpisz tytul");
					String tytul = scanner.nextLine();

					ksiazki.updateOne(eq("_id", id), new Document("$set", new Document("imie", imie).append("nazwisko", nazwisko).append("tytul", tytul)));
					System.out.println("Ksiazka o id " + id + " Zaktualizowana");
				}
				else if(wybor==2){
					System.out.println("Wpisz id pracownika");
					int id = Integer.parseInt(scanner.nextLine());

					System.out.println("Wpisz imie");
					String imie = scanner.nextLine();
					System.out.println("Wpisz nazwisko");
					String nazwisko = scanner.nextLine();
					System.out.println("Wpisz PESEL");
					String pesel = scanner.nextLine();

					employees.updateOne(eq("_id", id), new Document("$set", new Document("imie", imie).append("nazwisko", nazwisko).append("pesel", pesel)));
					System.out.println("Pracownik o id " + id + " Zaktualizowany");
				}

			}

		}

		mongoClient.close();
	}
}